CREATE DATABASE library
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Colombia.1252'
    LC_CTYPE = 'Spanish_Colombia.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE TABLE public.books
(
    id bigint NOT NULL DEFAULT nextval('books_id_seq'::regclass),
    cover_route text COLLATE pg_catalog."default",
    create_at timestamp without time zone,
    description character varying(255) COLLATE pg_catalog."default",
    name character varying(255) COLLATE pg_catalog."default",
    price integer NOT NULL,
    status character varying(255) COLLATE pg_catalog."default",
    stock double precision,
    CONSTRAINT books_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.books
    OWNER to postgres;
	
	
CREATE TABLE public.customers
(
    id bigint NOT NULL DEFAULT nextval('customers_id_seq'::regclass),
    create_at timestamp without time zone,
    name character varying(255) COLLATE pg_catalog."default",
    password character varying(255) COLLATE pg_catalog."default",
    status character varying(255) COLLATE pg_catalog."default",
    surname character varying(255) COLLATE pg_catalog."default",
    username character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT customers_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.customers
    OWNER to postgres;
	
	
CREATE TABLE public.shoppings
(
    id bigint NOT NULL DEFAULT nextval('shoppings_id_seq'::regclass),
    create_at timestamp without time zone,
    id_book bigint,
    id_customer bigint,
    price double precision,
    quantity integer NOT NULL,
    status character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT shoppings_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.shoppings
    OWNER to postgres;